﻿// See https://aka.ms/new-console-template for more information
using MRK;
using System.Xml.Linq;

Console.WriteLine("14 Варіант");

List<Doctor> doctors = new List<Doctor>();
List<HospitalRecord> hospitalRecords = new List<HospitalRecord>();

doctors.Add(new Doctor { LastName = "Доктор1", Specialization = "Педіатр" });
doctors.Add(new Doctor { LastName = "Доктор2", Specialization = "Терапевт" });

hospitalRecords.Add(new HospitalRecord { PatientLastName = "Пацієнт1", DoctorLastName = "Доктор1", Diagnosis = "Грип", AdmissionDate = new DateTime(2023, 1, 15), DischargeDate = new DateTime(2023, 1, 20) });
hospitalRecords.Add(new HospitalRecord { PatientLastName = "Пацієнт2", DoctorLastName = "Доктор2", Diagnosis = "Захворювання шлунку", AdmissionDate = new DateTime(2023, 2, 10), DischargeDate = new DateTime(2023, 2, 18) });

string patientLastNameToSearch = "Пацієнт1";
int currentYear = DateTime.Now.Year;

var ex = new Ex();

ex.taskA(hospitalRecords, patientLastNameToSearch, currentYear);
ex.taskB(hospitalRecords);
ex.taskC(doctors);

namespace MRK
{
    public class Doctor
    {
        public string LastName { get; set; }
        public string Specialization { get; set; }
    }

    public class HospitalRecord
    {
        public string PatientLastName { get; set; }
        public string DoctorLastName { get; set; }
        public string Diagnosis { get; set; }
        public DateTime AdmissionDate { get; set; }
        public DateTime DischargeDate { get; set; }
    }

    public class Ex
    {
        // Вивести прізвища лікарів, які ставили діагнози заданому пацієнту у поточному році
        public void taskA (List<HospitalRecord> hospitalRecords, string patientLastNameToSearch, int currentYear)
        {
            var doctorNames = hospitalRecords
                .Where(record => record.PatientLastName == patientLastNameToSearch && record.AdmissionDate.Year == currentYear)
                .Select(record => record.DoctorLastName)
                .Distinct();

            foreach (var doctorName in doctorNames)
            {
                Console.WriteLine(doctorName);
            }
        }

        // Вивести список прізвищ пацієнтів та сумарну тривалість їх перебування у лікарні
        public void taskB (List<HospitalRecord> hospitalRecords)
        {
            var patientRecords = hospitalRecords.GroupBy(record => record.PatientLastName);

            foreach (var group in patientRecords)
            {
                string patientName = group.Key;
                double totalStayDuration = group.Sum(record => (record.DischargeDate - record.AdmissionDate).TotalDays);

                Console.WriteLine($"{patientName}: {totalStayDuration} днів");
            }

        }

        // Зберегти дані однієї з колекцій в XML-файлі
        public void taskC (List<Doctor> doctors)
        {
            XElement doctorsXml = new XElement("Doctors",
                doctors.Select(doctor => new XElement("Doctor",
                    new XElement("LastName", doctor.LastName),
                    new XElement("Specialization", doctor.Specialization)
                ))
            );

            doctorsXml.Save("doctors.xml");
        }
    }

}

